﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_3.ControlePedido
{
    public class PedidoItemBusiness
    {
        PedidoItemDatabase db = new PedidoItemDatabase();

        public int Salvar(PedidoItemDTO dto)
        {
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<PedidoItemDTO> Consultar(string pedido)
        {
            return db.Consultar(pedido);
        }

        public List<PedidoItemDTO> Listar()
        {
            return db.Listar();
        }
    }
}
