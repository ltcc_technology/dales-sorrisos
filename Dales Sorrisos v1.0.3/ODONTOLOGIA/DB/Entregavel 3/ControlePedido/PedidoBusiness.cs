﻿using ODONTOLOGIA.DB.Entregavel_2.ControleProduto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_3.ControlePedido
{
    public class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            if (pedido.FormaPagamento == "Selecione")
            {
                throw new ArgumentException("Forma de Pagamento é obrigatório.");
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();

            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.Id = idPedido;
                itemDto.IdProduto = item.Id;
            }

            return idPedido;
        }
        public void Remover(int id)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Remover(id);

        }

        public List<PedidoDTO> Consultar(string pedido)
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Consultar(pedido);
        }

        public List<PedidoDTO> Listar()
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Listar();
        }
    }
}