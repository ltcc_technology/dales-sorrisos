﻿using MySql.Data.MySqlClient;
using ODONTOLOGIA.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_3.ControlePedido
{
    public class PedidoDatabase
    {

        public int Salvar(PedidoDTO pedido)
        {
            string script =
                @"INSERT INTO tb_pedido (id_pedido, id_paciente, nm_paciente, nm_tratamento, dt_venda, vl_total, ds_formapagamento)
                       VALUES (@id_pedido, @id_paciente, @nm_paciente, @nm_tratamento, @dt_venda, @vl_total, @ds_formapagamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", pedido.Id));
            parms.Add(new MySqlParameter("id_paciente", pedido.IdPaciente));
            parms.Add(new MySqlParameter("nm_paciente", pedido.Paciente));
            parms.Add(new MySqlParameter("nm_tratamento", pedido.Tratamento));
            parms.Add(new MySqlParameter("dt_venda", pedido.Data));
            parms.Add(new MySqlParameter("vl_total", pedido.Total));
            parms.Add(new MySqlParameter("ds_formapagamento", pedido.FormaPagamento));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_pedido WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<PedidoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoDTO> lista = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();

                dto.Id = reader.GetInt32("id_pedido");
                dto.Paciente = reader.GetString("nm_paciente");
                dto.Data = reader.GetDateTime("dt_venda");
                dto.Tratamento = reader.GetString("nm_tratamento");
                dto.Total = reader.GetDecimal("vl_total");
                dto.FormaPagamento = reader.GetString("ds_formapagamento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<PedidoDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM tb_pedido
                                WHERE nm_paciente like @nm_paciente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_paciente", "%" + cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoDTO> consulta = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();

                dto.Id = reader.GetInt32("id_pedido");
                dto.Paciente = reader.GetString("nm_paciente");
                dto.Data = reader.GetDateTime("dt_venda");
                dto.Tratamento = reader.GetString("nm_tratamento");
                dto.Total = reader.GetDecimal("vl_total");
                dto.FormaPagamento = reader.GetString("ds_formapagamento");

                consulta.Add(dto);
            }
            reader.Close();

            return consulta;
        }
    }
}
