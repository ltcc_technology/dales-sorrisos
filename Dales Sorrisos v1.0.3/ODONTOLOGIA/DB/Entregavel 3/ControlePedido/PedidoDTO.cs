﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_3.ControlePedido
{
    public class PedidoDTO
    {
        public int Id { get; set; }

        public int IdPaciente { get; set; }

        public string Paciente { get; set; }

        public string Tratamento { get; set; }

        public DateTime Data { get; set; }

        public decimal Total { get; set; }

        public string FormaPagamento { get; set; }
    }
}
