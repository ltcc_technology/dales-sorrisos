﻿using MySql.Data.MySqlClient;
using ODONTOLOGIA.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_2.ControleCompras
{
    public class CompraItemDatabase
    {
        public int Salvar(CompraItemDTO compras)
        {
            string script =
                @"INSERT INTO tb_compraitem (id_compraitem, id_compra, id_produto)
                    VALUES (@id_compraitem, @id_compra, @id_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compraitem", compras.CompraItem));
            parms.Add(new MySqlParameter("id_compra", compras.Compra));
            parms.Add(new MySqlParameter("id_produto", compras.Produto));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_compraitem WHERE id_compraitem = @id_compraitem";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compraitem", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<CompraItemDTO> Listar()
        {
            string script =
                @"SELECT * FROM tb_compraitem";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CompraItemDTO> compras = new List<CompraItemDTO>();
            while (reader.Read())
            {

                CompraItemDTO novacompra = new CompraItemDTO();
                novacompra.CompraItem = reader.GetInt32("id_compraitem");
                novacompra.Compra = reader.GetInt32("id_compra");
                novacompra.Produto = reader.GetInt32("id_produto");

                compras.Add(novacompra);

            }
            reader.Close();

            return compras;
        }

        public List<CompraItemDTO> Consultar(string produto)
        {

            string script =
                @"SELECT * FROM tb_compraitem
                  WHERE id_produto like @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", "%" + produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CompraItemDTO> compras = new List<CompraItemDTO>();

            while (reader.Read())
            {

                CompraItemDTO novacompra = new CompraItemDTO();

                novacompra.CompraItem = reader.GetInt32("id_compraitem");
                novacompra.Compra = reader.GetInt32("id_compra");
                novacompra.Produto = reader.GetInt32("id_produto");

                compras.Add(novacompra);

            }
            reader.Close();

            return compras;
        }
    }
}

