﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_2.ControleCompras
{
   public class ComprasBusiness
    {
        ComprasDatabase db = new ComprasDatabase();

        public int Salvar(ComprasDTO compra)
        {
            if (compra.Nome == string.Empty)
            {
                throw new ArgumentException("Nome da compra é obrigatório.");
            }
            if (compra.Quantidade == 0)
            {
                throw new ArgumentException("Quantidade é obrigatória.");
            }
            if (compra.Total <= 0)
            {
                throw new ArgumentException("Valor da compra é obrigatório.");
            }

            return db.Salvar(compra);

        }
        public int SalvarItem(CompraItemDTO produto, List<ComprasDTO> compras)
        {

            int idproduto = produto.Produto;


            CompraItemBusiness itemBusiness = new CompraItemBusiness();

            foreach (ComprasDTO item in compras)
            {
                CompraItemDTO itemDto = new CompraItemDTO();
                itemDto.Compra = item.Id;
                itemDto.Produto = idproduto;

                itemBusiness.Salvar(itemDto);
            }

            return idproduto;
        }

        public int DarBaixa(ComprasDTO dto)
        {
            ComprasDatabase db = new ComprasDatabase();
            return db.DarBaixaEstoque(dto);
        }

        public List<ComprasDTO> Consultar(string compra)
        {
            return db.Consultar(compra);
        }

        public List<ComprasDTO> Listar()
        {
            return db.Listar();
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
    }
}

