﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_2.ControleCompras
{
    public class ComprasDTO
    {
        public int Id { get; set; }
        public int IdFornec { get; set; }

        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public decimal Total { get; set; }
        public DateTime Data { get; set; }
    }
}
