﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_2.ControleCompras
{
    public class CompraItemDTO
    {
        public int CompraItem { get; set; }
        public int Compra { get; set; }
        public int Produto { get; set; }
    }
}
