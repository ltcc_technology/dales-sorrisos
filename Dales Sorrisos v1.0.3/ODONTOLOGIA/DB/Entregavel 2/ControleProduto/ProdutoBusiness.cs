﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_2.ControleProduto
{
   public class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();

        public int Salvar(ProdutoDTO produto)
        {
            if (produto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (produto.Descricao == string.Empty)
            {
                throw new ArgumentException("Descrição é obrigatório.");
            }
            if (produto.Valor <= 0)
            {
                throw new ArgumentException("Valor do produto é obrigatório.");
            }

            return db.Salvar(produto);

        }
        
        public List<ProdutoDTO> Consultar(string produto)
        {
            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Listar()
        {
            return db.Listar();
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
    }
}

