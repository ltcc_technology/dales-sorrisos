﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_1.FolhadePagamento
{
    public class VwConsultarFolhapagamento
    {
        public string Nome { get; set; }

        public string Mês { get; set; }

        public decimal SalarioLiquido { get; set; }
    }
}
