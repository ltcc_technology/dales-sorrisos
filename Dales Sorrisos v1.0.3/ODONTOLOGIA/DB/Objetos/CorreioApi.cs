﻿using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace ODONTOLOGIA.DB.Objetos
{
    class CorreioApi
    {
        public CorreioResponde Buscar(string cep)
        {
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

            CorreioResponde correio = JsonConvert.DeserializeObject<CorreioResponde>(resposta);
            return correio;
        }
    }
}
