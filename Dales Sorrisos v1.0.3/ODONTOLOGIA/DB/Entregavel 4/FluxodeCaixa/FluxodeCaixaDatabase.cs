﻿using MySql.Data.MySqlClient;
using ODONTOLOGIA.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_4.FluxodeCaixa
{
    public class FluxodeCaixaDatabase
    {
        public List<FluxodeCaixaDTO> Filtro(DateTime start, DateTime end)
        {

            string script = @"select * from fluxo_view where dt_data >= @start and @end <= dt_data";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("start", start));
            parms.Add(new MySqlParameter("end", end));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FluxodeCaixaDTO> lista = new List<FluxodeCaixaDTO>();
            while (reader.Read() == true)
            {
                FluxodeCaixaDTO vw = new FluxodeCaixaDTO();
                vw.Data = reader.GetDateTime("dt_data");
                vw.Total = reader.GetDecimal("vl_total");
                vw.Operacao = reader.GetString("ds_caixa");
                vw.Movimento = reader.GetString("ds_movimento");


                lista.Add(vw);
            }
            return lista;
        }
    }
}
