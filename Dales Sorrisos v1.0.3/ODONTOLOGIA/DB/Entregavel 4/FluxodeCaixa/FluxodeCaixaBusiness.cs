﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_4.FluxodeCaixa
{
    public class FluxodeCaixaBusiness
    {
        public List<FluxodeCaixaDTO> filtro(DateTime start, DateTime end)
        {
            FluxodeCaixaDatabase db = new FluxodeCaixaDatabase();
            List<FluxodeCaixaDTO> dados = db.Filtro(start, end);
            return dados;
        }
    }
}
