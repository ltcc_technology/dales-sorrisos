﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODONTOLOGIA.DB.Entregavel_4.FluxodeCaixa
{
    public class FluxodeCaixaDTO
    {
        public DateTime Data { get; set; }

        public decimal Total { get; set; }

        public string Movimento { get; set; }

        public string Operacao { get; set; }
    }
}
