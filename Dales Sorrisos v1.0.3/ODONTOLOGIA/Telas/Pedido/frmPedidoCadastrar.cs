﻿using ODONTOLOGIA.DB.Entregavel_2.ControleProduto;
using ODONTOLOGIA.DB.Entregavel_3.ControlePedido;
using ODONTOLOGIA.DB.Pacientes;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.Pedido
{
    public partial class frmPedidoCadastrar : Form
    {
        BindingList<ProdutoDTO> produtos = new BindingList<ProdutoDTO>();

        decimal valordavenda = 0;

        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {

            PacientesBusiness buss = new PacientesBusiness();
            List<PacientesDTO> listas = buss.Listar();
            cboPaciente.ValueMember = nameof(PacientesDTO.Id);
            cboPaciente.DisplayMember = nameof(PacientesDTO.Nome);
            cboPaciente.DataSource = listas;

            ProdutoBusiness busi = new ProdutoBusiness();
            List<ProdutoDTO> list = busi.Listar();
            cboTratamento.ValueMember = nameof(ProdutoDTO.Id);
            cboTratamento.DisplayMember = nameof(ProdutoDTO.Nome);
            cboTratamento.DataSource = list;

            ProdutoBusiness bus = new ProdutoBusiness();
            List<ProdutoDTO> lista = busi.Listar();
            cboValor.ValueMember = nameof(ProdutoDTO.Id);
            cboValor.DisplayMember = nameof(ProdutoDTO.Valor);
            cboValor.DataSource = list;

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            frmPedidoConsultar tela = new frmPedidoConsultar();
            tela.Show();
            Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                PacientesDTO cliente = cboPaciente.SelectedItem as PacientesDTO;

                PedidoDTO dto = new PedidoDTO();

                dto.IdPaciente = cliente.Id;
                dto.Paciente = cboPaciente.Text;
                dto.Tratamento = cboTratamento.Text;
                dto.Data = DateTime.Now;
                dto.Total = Convert.ToDecimal(cboValor.Text);
                dto.FormaPagamento = cboFormadePag.Text;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtos.ToList());

                MessageBox.Show("Pedido salvo com sucesso.", "Dales Sorrisos",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
