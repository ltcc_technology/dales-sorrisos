﻿using ODONTOLOGIA.DB.Entregavel_2.ControleFornecedor;
using ODONTOLOGIA.DB.Objetos;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.Fornecedor
{
    public partial class frmFornecedorCadastrar : Form
    {
        public frmFornecedorCadastrar()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            try
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Nome = txtNome.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.Email = txtEmail.Text.Trim();
                dto.CEP = txtCEP.Text.Trim();
                dto.CNPJ = txtCNPJ.Text.Trim();
                dto.Rua = txtRua.Text.Trim();
                dto.NdaCasa = txtNumero.Text.Trim();
                

               FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);

                MessageBox.Show("Fornecedor salvo com sucesso.", "Dales Sorrisos",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            frmFornecedorConsultar tela = new frmFornecedorConsultar();
            tela.Show();
            Hide();
        }

        private void btnCep_Click(object sender, EventArgs e)
        {
            CorreioApi correioApi = new CorreioApi();

            string cep = txtCEP.Text.Trim().Replace("-", "");

            CorreioResponde correio = correioApi.Buscar(cep);

            txtRua.Text = correio.logradouro;
        }
    }
}
