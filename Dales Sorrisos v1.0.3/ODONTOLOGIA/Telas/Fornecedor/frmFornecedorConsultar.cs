﻿using ODONTOLOGIA.DB.Entregavel_2.ControleFornecedor;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.Fornecedor
{
    public partial class frmFornecedorConsultar : Form
    {
        public frmFornecedorConsultar()
        {
            InitializeComponent();
        }
        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        } 

        private void btnApagar_Click(object sender, EventArgs e)
        {
            FornecedorDTO fornecedor = dgvConsultarFornecedor.CurrentRow.DataBoundItem as FornecedorDTO;

            DialogResult r = MessageBox.Show("Deseja excluir esse Fornecedor?", "Dales Sorrisos",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (r == DialogResult.Yes)
            {

                FornecedorDTO dto = dgvConsultarFornecedor.CurrentRow.DataBoundItem as FornecedorDTO;
                FornecedorBusiness business = new FornecedorBusiness();
                business.Remover(dto.Id);
                btnConsultar_Click(null, null);
            }

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorBusiness business = new FornecedorBusiness();
                List<FornecedorDTO> lista = business.Consultar(string.Empty);
                dgvConsultarFornecedor.AutoGenerateColumns = false;
                dgvConsultarFornecedor.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
