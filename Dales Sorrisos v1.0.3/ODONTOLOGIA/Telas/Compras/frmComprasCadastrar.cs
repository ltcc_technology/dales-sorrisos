﻿using ODONTOLOGIA.DB.Entregavel_2.ControleCompras;
using ODONTOLOGIA.DB.Entregavel_2.ControleFornecedor;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.Compras
{
    public partial class frmComprasCadastrar : Form
    {
        public frmComprasCadastrar()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> Funcionarios = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFornecedor.DataSource = Funcionarios;

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = cboFornecedor.SelectedItem as FornecedorDTO;
                ComprasDTO dto = new ComprasDTO();
                dto.IdFornec = fornecedor.Id;
                dto.Nome = txtNome.Text;
                dto.Quantidade = Convert.ToInt32(txtQuantidade.Text);
                dto.Total = Convert.ToDecimal(txtTotal.Text);
                dto.Data = DateTime.Now;

                ComprasBusiness bussiness = new ComprasBusiness();
                bussiness.Salvar(dto);
                    
                MessageBox.Show("Compra efetuada com sucesso.", "Dales Sorrisos",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}