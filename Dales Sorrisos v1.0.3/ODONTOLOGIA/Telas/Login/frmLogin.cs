﻿using ODONTOLOGIA.DB.Funcionario;
using ODONTOLOGIA.Telas.EsqueceuSenha;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODONTOLOGIA
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtUsuario.Text, txtSenha.Text);

                if (funcionario != null)
                {
                    UserSession.UsuarioLogado = funcionario;

                    trelasodo.FrmMenu tela = new trelasodo.FrmMenu();
                    tela.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Dales Sorriso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
        }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
}

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblDados_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            EsqueciSenha tela = new EsqueciSenha();
            tela.Show();
            Hide();
        }
    }
}