﻿using ODONTOLOGIA.DB.Entregavel_1.FolhadePagamento;
using ODONTOLOGIA.Folha_de_pagamento;
using ODONTOLOGIA.Folha_de_Pagamento;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODONTOLOGIA.RH
{
    public partial class Consultar_pagamennto : Form
    {
        public Consultar_pagamennto()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            FolhaPagamentoBusiness business = new FolhaPagamentoBusiness();
            List<VwConsultarFolhapagamento> a = business.Consultarmes(txtConsultar.Text);
            dgvConsultarFolha.AutoGenerateColumns = false;
            dgvConsultarFolha.DataSource = a;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }
    }
}
