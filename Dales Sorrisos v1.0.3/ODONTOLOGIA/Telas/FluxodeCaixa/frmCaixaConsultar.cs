﻿using ODONTOLOGIA.DB.Entregavel_4.FluxodeCaixa;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.FluxodeCaixa
{
    public partial class frmCaixaConsultar : Form
    {
        public frmCaixaConsultar()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {

                FluxodeCaixaBusiness bus = new FluxodeCaixaBusiness();

                List<FluxodeCaixaDTO> vendas = bus.filtro(dtpInicio.Value.Date, dtoFim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59));

                dgvFluxoCaixa.DataSource = vendas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                decimal saldo = 0;
                decimal totalentrada = 0;
                decimal totalsaida = 0;

                foreach (DataGridViewRow row in dgvFluxoCaixa.Rows)
                {
                    FluxodeCaixaDTO dto = row.DataBoundItem as FluxodeCaixaDTO;

                    if (dto.Operacao == "Entrada")
                    {
                        decimal entrada = dto.Total;
                        totalentrada = entrada + totalentrada;
                    }
                    else
                    {
                        decimal saida = dto.Total;
                        totalsaida = saida + totalsaida;
                    }

                }
                lblEnt.Text = totalentrada.ToString();
                lblSai.Text = totalsaida.ToString();
                saldo = totalentrada - totalsaida;
                lblSit.Text = saldo.ToString();
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
