﻿using ODONTOLOGIA.Folha_de_pagamento;
using ODONTOLOGIA.Telas.Compras;
using ODONTOLOGIA.Telas.FluxodeCaixa;
using ODONTOLOGIA.Telas.Fornecedor;
using ODONTOLOGIA.Telas.Pedido;
using ODONTOLOGIA.Telas.Produto;
using System;
using System.Windows.Forms;

namespace ODONTOLOGIA.trelasodo
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
            VerificarPermissoes();
        }

        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.PermissaoAdm == false)
            {
                if (UserSession.UsuarioLogado.PermissaoAdm == false)
                {
                    folhaDePagamentoToolStripMenuItem.Enabled = false;
                }
                if (UserSession.UsuarioLogado.PermissaoAdm == false)
                {
                    funcionarioToolStripMenuItem.Enabled = false;
                }
                if (UserSession.UsuarioLogado.PermissaoAdm == false)
                {
                    fluxoDeCaiixaToolStripMenuItem.Enabled = false;
                }
                if (UserSession.UsuarioLogado.PermissaoAdm == false)
                {
                    estoqueToolStripMenuItem.Enabled = false;
                }
                if (UserSession.UsuarioLogado.PermissaoAdm == false)
                {
                    tratamentoToolStripMenuItem.Enabled = false;
                }
                if (UserSession.UsuarioLogado.PermissaoAdm == false)
                {
                    fornecedorToolStripMenuItem.Enabled = false;
                }
                if (UserSession.UsuarioLogado.PermissaoAdm == false)
                {
                    comprasToolStripMenuItem.Enabled = false;
                }
                if (UserSession.UsuarioLogado.PermissaoAdm == false)
                {
                    folhaDePagamentoToolStripMenuItem.Enabled = false;
                }
            }
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pacientes tela = new pacientes();
            tela.Show();
            Hide();
        }

        private void funcionarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            funcionario tela = new funcionario();
            tela.Show();
            Hide();
        }

        private void tratamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProdutoCadastrar tela = new frmProdutoCadastrar();
            tela.Show();
            Hide();
        }

        private void pedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar tela = new frmPedidoCadastrar();
            tela.Show();
            Hide();
        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFornecedorCadastrar tela = new frmFornecedorCadastrar();
            tela.Show();
            Hide();
        }

        private void comprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmComprasCadastrar tela = new frmComprasCadastrar();
            tela.Show();
            Hide();
        }

        private void fluxoDeCaiixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCaixaConsultar tela = new frmCaixaConsultar();
            tela.Show();
            Hide();
        }

        private void folhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Folha_De_Pagamento tela = new Folha_De_Pagamento();
            tela.Show();
            Hide();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmComprasConsultar tela = new frmComprasConsultar();
            tela.Show();
            Hide();
        }
    }
}
