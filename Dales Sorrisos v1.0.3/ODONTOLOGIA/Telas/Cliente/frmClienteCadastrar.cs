﻿using ODONTOLOGIA.DB.Objetos;
using ODONTOLOGIA.DB.Pacientes;
using ODONTOLOGIA.Telas.Cliente;
using System;
using System.Windows.Forms;

namespace ODONTOLOGIA.trelasodo
{
    public partial class pacientes : Form
    {
        public pacientes()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            try
            {
                PacientesDTO dto = new PacientesDTO();
                dto.Nome = mtbNome.Text.Trim();
                dto.Rg = mtbRG.Text.Trim();
                dto.Cpf = mtbCPF.Text.Trim();
                dto.Email = mtbEmail.Text.Trim();
                dto.DtNascimento = dtpNascimento.Value;
                dto.Telefone = mtbTelefone.Text.Trim();
                dto.Celular = mtbCelular.Text.Trim();
                dto.Cep = mtbCEP.Text.Trim();
                dto.Rua = mtbRua.Text.Trim();
                dto.NdaCasa = mtbNumero.Text.Trim();
                dto.Complemento = mtbComplemento.Text.Trim();

                PacientesBusiness business = new PacientesBusiness();
                business.Salvar(dto);

                MessageBox.Show("Paciente salvo com sucesso.", "Dales Sorrisos",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            frmClienteConsultar tela = new frmClienteConsultar();
            tela.Show();
            Hide();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }

        private void btnCep_Click(object sender, EventArgs e)
        {
            CorreioApi correioApi = new CorreioApi();

            string cep = mtbCEP.Text.Trim().Replace("-", "");

            CorreioResponde correio = correioApi.Buscar(cep);

            mtbRua.Text = correio.logradouro;
        }
    }
}
