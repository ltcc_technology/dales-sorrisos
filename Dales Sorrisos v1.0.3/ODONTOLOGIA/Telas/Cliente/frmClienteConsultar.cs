﻿using ODONTOLOGIA.DB.Pacientes;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.Cliente
{
    public partial class frmClienteConsultar : Form
    {
        public frmClienteConsultar()
        {
            InitializeComponent();
        }

        private void btnConsultarClientes_Click(object sender, EventArgs e)
        {
            PacientesBusiness business = new PacientesBusiness();
            List<PacientesDTO> a = business.Consultar(txtConsultar.Text);
            dgvConsultar.AutoGenerateColumns = false;
            dgvConsultar.DataSource = a;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PacientesDTO cliente = dgvConsultar.CurrentRow.DataBoundItem as PacientesDTO;

                DialogResult r = MessageBox.Show("Deseja excluir esse Paciente?", "Dales Sorrisos",
                                       MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    PacientesBusiness business = new PacientesBusiness();
                    business.Remover(cliente.Id);

                    List<PacientesDTO> a = business.Consultar(txtConsultar.Text);
                    dgvConsultar.AutoGenerateColumns = false;
                    dgvConsultar.DataSource = a;

                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnSair_Click_1(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }
    }
}
