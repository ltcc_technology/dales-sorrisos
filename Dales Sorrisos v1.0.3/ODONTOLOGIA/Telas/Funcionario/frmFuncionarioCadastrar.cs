﻿using ODONTOLOGIA.DB.Funcionario;
using ODONTOLOGIA.RH;
using System;
using System.Windows.Forms;

namespace ODONTOLOGIA.trelasodo
{
    public partial class funcionario : Form
    {
        public funcionario()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();

                dto.Nome = txtNome.Text.Trim();
                dto.Sexo = cboSexo.Text.Trim();
                dto.DtNascimento = dtpNasc.Value;
                dto.Rg = mtbRG.Text.Trim();
                dto.Cpf = mtbCPF.Text.Trim();
                dto.Telefone = mtbTelefone.Text.Trim();
                dto.Cep = mtbCEP.Text.Trim();
                dto.NdaCasa = txtNumero.Text.Trim();
                dto.Email = txtEmail.Text.Trim();
                dto.DataAdm = dtpAdmissao.Value;
                dto.Cargo = txtCargo.Text.Trim();
                dto.Login = txtLogin.Text.Trim();
                dto.Senha = txtSenha.Text.Trim();
                dto.Salario = Convert.ToDecimal(txtSalario.Text.Trim());

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("Funcionário salvo com sucesso.", "Dales Sorrisos",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar_Funcionario tela = new Consultar_Funcionario();
            tela.Show();
            Hide();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }
    }
}
