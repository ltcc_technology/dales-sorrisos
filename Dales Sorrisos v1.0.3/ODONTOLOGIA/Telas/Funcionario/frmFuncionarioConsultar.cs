﻿using ODONTOLOGIA.DB.Funcionario;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ODONTOLOGIA.RH
{
    public partial class Consultar_Funcionario : Form
    {
        public Consultar_Funcionario()
        {
            InitializeComponent();
        }


        private void btnConsultar_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> a = business.Consultar(txtConsultar.Text);
            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = a;
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO funcionario = dgvFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja excluir esse Funcionário?", "Dales Sorrisos",
                                       MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(funcionario.Id);

                    List<FuncionarioDTO> a = business.Consultar(txtConsultar.Text);
                    dgvFuncionario.AutoGenerateColumns = false;
                    dgvFuncionario.DataSource = a;

                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }
    }
}