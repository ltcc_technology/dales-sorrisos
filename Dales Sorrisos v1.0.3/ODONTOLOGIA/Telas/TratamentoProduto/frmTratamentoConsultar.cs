﻿using ODONTOLOGIA.DB.Entregavel_2.ControleProduto;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.Produto
{
    public partial class frmProdutoConsultar : Form
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ProdutoBusiness Business = new ProdutoBusiness();
            List<ProdutoDTO> a = Business.Consultar(txtConsultar.Text);
            dgvConsultarProdutos.AutoGenerateColumns = false;
            dgvConsultarProdutos.DataSource = a;

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarProdutos.CurrentRow != null)
                {
                    ProdutoDTO produto = dgvConsultarProdutos.CurrentRow.DataBoundItem as ProdutoDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir esse Tratamento?", "Dales Sorrisos",
                                           MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        ProdutoBusiness business = new ProdutoBusiness();
                        business.Remover(produto.Id);

                        List<ProdutoDTO> a = business.Consultar(txtConsultar.Text);
                        dgvConsultarProdutos.AutoGenerateColumns = false;
                        dgvConsultarProdutos.DataSource = a;

                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
