﻿using ODONTOLOGIA.DB.Entregavel_2.ControleCompras;
using ODONTOLOGIA.DB.Entregavel_2.ControleProduto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.Compras
{
    public partial class frmDarBaixa : Form
    {
        public frmDarBaixa()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {

            ComprasBusiness buss = new ComprasBusiness();
            List<ComprasDTO> listas = buss.Listar();
            cboProduto.ValueMember = nameof(ComprasDTO.Id);
            cboProduto.DisplayMember = nameof(ComprasDTO.Nome);
            cboProduto.DataSource = listas;
        }

            private void btnBaixa_Click(object sender, EventArgs e)
        {
            {
                ComprasDTO cat = cboProduto.SelectedItem as ComprasDTO;

                ComprasDTO dto = new ComprasDTO();

                dto.Id = cat.Id;
                dto.Quantidade = Convert.ToInt32(txtQuantidade.Text.Trim());

                ComprasBusiness a = new ComprasBusiness();
                a.DarBaixa(dto);

                MessageBox.Show("Estoque alterado com sucesso.", "Dales Sorrisos",
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Information);
            }
        }

        private void btnSair_Click_1(object sender, EventArgs e)
        {
            frmComprasConsultar tela = new frmComprasConsultar();
            tela.Show();
            Hide();
        }
    }
}
