﻿using ODONTOLOGIA.DB.Entregavel_2.ControleCompras;
using ODONTOLOGIA.trelasodo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODONTOLOGIA.Telas.Compras
{
    public partial class frmComprasConsultar : Form
    {
        public frmComprasConsultar()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            FrmMenu tela = new FrmMenu();
            tela.Show();
            Hide();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ComprasBusiness business = new ComprasBusiness();
            List<ComprasDTO> a = business.Consultar(txtConsultar.Text);
            dgvconsultacompra.AutoGenerateColumns = false;
            dgvconsultacompra.DataSource = a;
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvconsultacompra.CurrentRow != null)
                {
                    ComprasDTO compra = dgvconsultacompra.CurrentRow.DataBoundItem as ComprasDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir essa Compra?", "Dales Sorrisos",
                                           MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        ComprasBusiness business = new ComprasBusiness();
                        business.Remover(compra.Id);

                        List<ComprasDTO> a = business.Consultar(txtConsultar.Text);
                        dgvconsultacompra.AutoGenerateColumns = false;
                        dgvconsultacompra.DataSource = a;

                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Dales Sorrisos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDarBaixa_Click(object sender, EventArgs e)
        {
            frmDarBaixa tela = new frmDarBaixa();
            tela.Show();
            Hide();
        }
    }
}
